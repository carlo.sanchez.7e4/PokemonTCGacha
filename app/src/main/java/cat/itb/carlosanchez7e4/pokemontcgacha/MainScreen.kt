package cat.itb.carlosanchez7e4.pokemontcgacha

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity

class MainScreen : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_menu)
    }
}