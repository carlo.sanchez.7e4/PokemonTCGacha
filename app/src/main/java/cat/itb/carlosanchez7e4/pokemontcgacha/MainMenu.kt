package cat.itb.carlosanchez7e4.pokemontcgacha

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import com.google.android.material.dialog.MaterialAlertDialogBuilder

class MainMenu : AppCompatActivity() {

    lateinit var startButton: Button
    lateinit var infoButton: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_menu)

        startButton = findViewById(R.id.startButton)
        infoButton = findViewById(R.id.infoButton)

        startButton.setOnClickListener{
            startActivity(Intent(this, MainScreen::class.java))
        }

        infoButton.setOnClickListener{
            MaterialAlertDialogBuilder(this,
                R.style.Theme_PokemonTCGacha)
                .setMessage(resources.getString(R.string.info_text))
                .setPositiveButton(resources.getString(R.string.got_it)) { dialog, which ->
                    // Respond to positive button press
                }
                .show()
        }
    }
}